package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParserFactory;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	List<Flower>flowers=new ArrayList<>();
	Flower flower;
	VisualParameters visualParameters;
	GrowingTips growingTips;
	String currentTagName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	public static void sortFlowers(List<Flower> flowerList) {
		Collections.sort(flowerList, new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getOrigin().compareTo(o2.getOrigin());
			}
		});
	}




	public List<Flower> getFlowers() {
		return flowers;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//		super.startElement(uri, localName, qName, attributes);=
		currentTagName=qName;
		if (currentTagName.equals("flower")){
			flower=new Flower();
		}else if (currentTagName.equals("visualParameters")){
			visualParameters=new VisualParameters();
		}else if (currentTagName.equals("growingTips")){
			growingTips=new GrowingTips();
		}else if (currentTagName.equals("aveLenFlower")){
			visualParameters.setAveLenFloverMesure(attributes.getValue("measure"));
		}else if (currentTagName.equals("tempreture")){
			growingTips.setTemperatureMeasure(attributes.getValue("measure"));
		}else if (currentTagName.equals("lighting")){
			growingTips.setLigthRequering(attributes.getValue("lightRequiring"));
		}else if (currentTagName.equals("watering")){
			growingTips.setWateringMeasure(attributes.getValue("measure"));
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
//		super.endElement(uri, localName, qName);
//		System.out.println("end");
		currentTagName=null;
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
//		super.characters(ch, start, length);

		if (currentTagName==null){
			return;
		}
		if (currentTagName.equals("name")){
			flower.setName(new String(ch,start,length));
		}else if (currentTagName.equals("soil")){
			flower.setSoil(new String(ch,start,length));
		}else if (currentTagName.equals("origin")){
			flower.setOrigin(new String(ch,start,length));
		}else if (currentTagName.equals("stemColour")){
			visualParameters.setStemColour(new String(ch,start,length));
		}else if (currentTagName.equals("leafColour")){
			visualParameters.setLeafColour(new String(ch,start,length));
		}else if (currentTagName.equals("aveLenFlower")) {
			String value=new String(ch,start,length);
			int aveLenFloverValue = Integer.parseInt(value);
			visualParameters.setAveLenFloverValue(aveLenFloverValue);
			flower.setVisualParameters(visualParameters);
		}else if (currentTagName.equals("tempreture")) {
			growingTips.setTemperatureValue(Integer.valueOf(new String(ch, start, length)));
		}else if (currentTagName.equals("watering")) {
			growingTips.setWateringValue(Integer.valueOf(new String(ch,start,length)));
			flower.setGrowingTips(growingTips);
		}else if (currentTagName.equals("multiplying")){
			flower.setMultiplying(new String(ch,start,length));
			flowers.add(flower);
		}



	}


}