package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public static void sortFlowers(List<Flower> flowerList){
		 Collections.sort(flowerList, new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});


	}
	public static List<Flower> parse(String fileName) {

		Flower flower = null;
		VisualParameters visualParameters = null;
		GrowingTips growingTips=null;
		List<Flower> flowerList = new ArrayList<>();
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

		try {

			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));
			while (reader.hasNext()) {
				XMLEvent xmlEvent = reader.nextEvent();
				if (xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement();
					if (startElement.getName().getLocalPart().equals("flower")) {
						flower = new Flower();
					} else if (startElement.getName().getLocalPart().equals("name")) {
						xmlEvent = reader.nextEvent();
						flower.setName(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("soil")) {
						xmlEvent = reader.nextEvent();
						flower.setSoil(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("origin")) {
						xmlEvent = reader.nextEvent();
						flower.setOrigin(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("visualParameters")) {
						visualParameters = new VisualParameters();
					} else if (startElement.getName().getLocalPart().equals("stemColour")) {
						xmlEvent = reader.nextEvent();
						visualParameters.setStemColour(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("leafColour")) {
						xmlEvent = reader.nextEvent();
						visualParameters.setLeafColour(xmlEvent.asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equals("aveLenFlower")) {
						Attribute measure = startElement.getAttributeByName(new QName("measure"));
						if (measure != null) {
							visualParameters.setAveLenFloverMesure(measure.getValue());
						}
						xmlEvent = reader.nextEvent();
						visualParameters.setAveLenFloverValue(Integer.parseInt(xmlEvent.asCharacters().getData()));
						flower.setVisualParameters(visualParameters);
					} else if (startElement.getName().getLocalPart().equals("growingTips")) {
						growingTips = new GrowingTips();
					} else if (startElement.getName().getLocalPart().equals("tempreture")) {
						Attribute measure = startElement.getAttributeByName(new QName("measure"));
						if (measure != null) {
							growingTips.setTemperatureMeasure(measure.getValue());
						}
						xmlEvent = reader.nextEvent();
						growingTips.setTemperatureValue(Integer.parseInt(xmlEvent.asCharacters().getData()));
					} else if (startElement.getName().getLocalPart().equals("lighting")) {
						Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
						if (lightRequiring != null)
							growingTips.setLigthRequering(lightRequiring.getValue());

					} else if (startElement.getName().getLocalPart().equals("watering")) {
						Attribute measure = startElement.getAttributeByName(new QName("measure"));
						if (measure != null) {
							growingTips.setWateringMeasure(measure.getValue());
						}
						xmlEvent = reader.nextEvent();
						growingTips.setWateringValue(Integer.parseInt(xmlEvent.asCharacters().getData()));
						flower.setGrowingTips(growingTips);
					} else if (startElement.getName().getLocalPart().equals("multiplying")) {
						xmlEvent = reader.nextEvent();
						flower.setMultiplying(xmlEvent.asCharacters().getData());
					}
				}
				if (xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						flowerList.add(flower);
					}
				}
			}
		}catch (FileNotFoundException | XMLStreamException e){
			e.printStackTrace();
		}

		return flowerList;
}

public static void saveToXML(String fileName,List<Flower>flowerList){
		Flower flower=null;
		try {
			XMLOutputFactory outputFactory=XMLOutputFactory.newInstance();
			XMLStreamWriter writer= outputFactory.createXMLStreamWriter(new FileWriter(fileName));
			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeStartElement("flowers");
			writer.writeAttribute("xmlns", "http://www.nure.ua");
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
			for (int i = 0; i < 2; i++) {
				flower=flowerList.get(i);
				writer.writeStartElement("flower");

				writer.writeStartElement("name");
				writer.writeCharacters(flower.getName());
				writer.writeEndElement();

				writer.writeStartElement("soil");
				writer.writeCharacters(flower.getSoil());
				writer.writeEndElement();

				writer.writeStartElement("origin");
				writer.writeCharacters(flower.getOrigin());
				writer.writeEndElement();

				writer.writeStartElement("visualParameters");

				writer.writeStartElement("stemColour");
				writer.writeCharacters(flower.getVisualParameters().getStemColour());
				writer.writeEndElement();

				writer.writeStartElement("leafColour");
				writer.writeCharacters(flower.getVisualParameters().getLeafColour());
				writer.writeEndElement();

				writer.writeStartElement("aveLenFlower");
				writer.writeAttribute("measure", flower.getVisualParameters().getAveLenFloverMesure());
				writer.writeCharacters(String.valueOf(flower.getVisualParameters().getAveLenFloverValue()));
				writer.writeEndElement();

				writer.writeEndElement();


				writer.writeStartElement("growingTips");


				writer.writeStartElement("tempreture");
				writer.writeAttribute("measure", flower.getGrowingTips().getTemperatureMeasure());
				writer.writeCharacters(String.valueOf(flower.getGrowingTips().getTemperatureValue()));
				writer.writeEndElement();

				writer.writeEmptyElement("lighting");
				writer.writeAttribute("lightRequiring",flower.getGrowingTips().getLigthRequering());

				writer.writeStartElement("watering");
				writer.writeAttribute("measure", flower.getGrowingTips().getWateringMeasure());
				writer.writeCharacters(String.valueOf(flower.getGrowingTips().getWateringValue()));
				writer.writeEndElement();

				writer.writeEndElement();

				writer.writeStartElement("multiplying");
				writer.writeCharacters(flower.getMultiplying());
				writer.writeEndElement();

				writer.writeEndElement();



			}

			writer.writeEndElement();
			writer.writeEndDocument();
			writer.flush();
		}catch (XMLStreamException | IOException exception){
			exception.printStackTrace();
		}
}
}