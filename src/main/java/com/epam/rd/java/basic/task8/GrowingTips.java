package com.epam.rd.java.basic.task8;

public class GrowingTips {

        String temperatureMeasure;
        int temperatureValue;
        String ligthRequering;
        String wateringMeasure;
        int wateringValue;

        public void setTemperatureMeasure(String temperatureMeasure) {
            this.temperatureMeasure = temperatureMeasure;
        }

        public void setTemperatureValue(int temperatureValue) {
            this.temperatureValue = temperatureValue;
        }

        public void setLigthRequering(String ligthRequering) {
            this.ligthRequering = ligthRequering;
        }

        public void setWateringMeasure(String wateringMeasure) {
            this.wateringMeasure = wateringMeasure;
        }

        public void setWateringValue(int wateringValue) {
            this.wateringValue = wateringValue;
        }


    public String getTemperatureMeasure() {
        return temperatureMeasure;
    }

    public int getTemperatureValue() {
        return temperatureValue;
    }

    public String getLigthRequering() {
        return ligthRequering;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public int getWateringValue() {
        return wateringValue;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperatureMeasure='" + temperatureMeasure + '\'' +
                ", temperatureValue=" + temperatureValue +
                ", ligthRequering='" + ligthRequering + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                ", wateringValue=" + wateringValue +
                '}';
    }
}
