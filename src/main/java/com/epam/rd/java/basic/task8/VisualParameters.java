package com.epam.rd.java.basic.task8;

public class VisualParameters {
        String stemColour;
        String leafColour;
        String aveLenFloverMesure;
        int aveLenFloverValue;

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public void setAveLenFloverMesure(String aveLenFloverMesure){
            this.aveLenFloverMesure=aveLenFloverMesure;
        }

        public void setAveLenFloverValue(int aveLenFloverValue){
            this.aveLenFloverValue=aveLenFloverValue;
        }


    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public String getAveLenFloverMesure() {
        return aveLenFloverMesure;
    }

    public int getAveLenFloverValue() {
        return aveLenFloverValue;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFloverMesure='" + aveLenFloverMesure + '\'' +
                ", aveLenFloverValue=" + aveLenFloverValue +
                '}';
    }
}


