package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);


		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE

		List<Flower>flowers2=domController.parse(xmlFileName);

		// sort (case 1)
		// PLACE YOUR CODE HERE

		domController.sortFlowers(flowers2);
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE


		STAXController stax=new STAXController(xmlFileName);
		stax.saveToXML(outputXmlFile,flowers2);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE

		SAXParserFactory factory=SAXParserFactory.newInstance();
		SAXParser parser=factory.newSAXParser();
		parser.parse(xmlFileName,saxController);
		List<Flower>flowers3=saxController.getFlowers();
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE

		saxController.sortFlowers(flowers3);
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE

		stax.saveToXML(outputXmlFile,flowers3);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE

		List<Flower>flowers=staxController.parse(xmlFileName);
		staxController.sortFlowers(flowers);


		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.saveToXML(outputXmlFile,flowers);
	}

}
