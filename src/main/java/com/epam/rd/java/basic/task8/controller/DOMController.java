package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	public static void sortFlowers(List<Flower> flowerList) {
		Comparator<Flower>flowerComparator=Comparator
												.comparing(Flower::getSoil)
												.thenComparing(Flower::getName);
		Collections.sort(flowerList, flowerComparator);
	}



	public static List<Flower> parse(String fileName){
		List<Flower>flowerList=new ArrayList<>();
		Flower flower;
		VisualParameters visualParameters;
		GrowingTips growingTips;
		File file=new File(fileName);
		DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
		Document doc= null;
		try {
			doc = dbf.newDocumentBuilder().parse(file);
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}


		NodeList flowerNods=doc.getElementsByTagName("flower");
		for (int i = 0; i < flowerNods.getLength(); i++) {
			flower = new Flower();
			Node flowerNode = flowerNods.item(i);
			if (flowerNode.getNodeType() == Node.ELEMENT_NODE) {
				NodeList info = flowerNode.getChildNodes();
				for (int j = 0; j < info.getLength(); j++) {
					Node infoNode = info.item(j);
					if (infoNode.getNodeType() == Node.ELEMENT_NODE) {
						switch (infoNode.getNodeName()) {
							case "name": {
								flower.setName(infoNode.getTextContent());
							}
							case "soil": {
								flower.setSoil(infoNode.getTextContent());
							}
							case "origin": {
								flower.setOrigin(infoNode.getTextContent());
							}
							case "visualParameters": {
								visualParameters = new VisualParameters();
								NodeList parameters = infoNode.getChildNodes();
								for (int k = 0; k < parameters.getLength(); k++) {
									Node parameter = parameters.item(k);
									if (parameter.getNodeType() == Node.ELEMENT_NODE) {
										switch (parameter.getNodeName()) {
											case "stemColour": {
												visualParameters.setStemColour(parameter.getTextContent());
												break;
											}
											case "leafColour": {
												visualParameters.setLeafColour(parameter.getTextContent());
												break;
											}
											case "aveLenFlower": {
												Element measure = (Element) parameter;
												visualParameters.setAveLenFloverMesure(measure.getAttribute("measure"));
												visualParameters.setAveLenFloverValue(Integer.parseInt(parameter.getTextContent()));
												flower.setVisualParameters(visualParameters);
												break;
											}
										}
									}
								}

							}
							case "growingTips": {
								growingTips = new GrowingTips();
								NodeList tips = infoNode.getChildNodes();
								for (int k = 0; k < tips.getLength(); k++) {
									Node tip = tips.item(k);
									if (tip.getNodeType() == Node.ELEMENT_NODE) {
										switch (tip.getNodeName()) {
											case "tempreture": {
												Element measure = (Element) tip;
												growingTips.setTemperatureMeasure(measure.getAttribute("measure"));
												growingTips.setTemperatureValue(Integer.parseInt(tip.getTextContent()));
											break;
											}
											case "lighting": {
												Element lighting = (Element) tip;
												growingTips.setLigthRequering(lighting.getAttribute("lightRequiring"));
											break;
											}
											case "watering": {
												Element measure = (Element) tip;
												growingTips.setWateringMeasure(measure.getAttribute("measure"));
												growingTips.setWateringValue(Integer.parseInt(tip.getTextContent()));
												flower.setGrowingTips(growingTips);
												break;
											}
										}
									}
								}
							}
							case "multiplying": {
								flower.setMultiplying(infoNode.getTextContent());
								flowerList.add(flower);
							}
						}
					}
				}
			}
		}
		return flowerList;
	}

}
